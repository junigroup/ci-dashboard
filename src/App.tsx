import { createMuiTheme } from '@material-ui/core';
import CssBaseline from '@material-ui/core/CssBaseline';
import { ThemeProvider } from '@material-ui/styles';
import React from 'react';
import './App.css';
import TopAppBar from './components/appbar';
import { JenkinsAccess, JenkinsConnector, JenkinsAccessContext } from './components/jenkins';
import { SettingsProvider } from './components/settings';
import { useSetting } from './components/settings/useSetting';
import { defaultSettings } from './constants';

const theme = createMuiTheme({

});

function WithJenkins({ children }: { children: React.ReactNode }) {
  const settings = useSetting(allSettings => allSettings.jenkins);
  return (
    <JenkinsConnector settings={settings}>
      {children}
    </JenkinsConnector>
  );
}

function App() {
  return (
    <SettingsProvider default={defaultSettings}>
      <JenkinsAccessContext.Provider value={new JenkinsAccess()}>
        <CssBaseline />
        <ThemeProvider theme={theme}>
          <WithJenkins>
            <div className="App">
              <TopAppBar />
            </div>
          </WithJenkins>
        </ThemeProvider>
      </JenkinsAccessContext.Provider>
    </SettingsProvider>
  );
}

export default App;
