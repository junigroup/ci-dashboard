import AppBar from '@material-ui/core/AppBar';
import { makeStyles } from '@material-ui/core/styles';
import Toolbar from '@material-ui/core/Toolbar';
import Typography, { TypographyProps } from '@material-ui/core/Typography';
import React from 'react';
import { useSetting } from '../settings/useSetting';
import FullscreenButton from './FullscreenButton';
import JenkinsButton from './JenkinsButton';
import SettingsButton from './SettingsButton';

const useStyles = makeStyles(theme => ({
    root: {
        flexGrow: 1,
    },
    menuButton: {
        marginRight: theme.spacing(2),
    },
    title: {
        flexGrow: 1,
    },
}));

const Title = (props: TypographyProps) => {
    const title = useSetting(it => it.dashboard && it.dashboard.title);
    return <Typography {...props}>{title}</Typography>
}

export default function TopAppBar() {
    const classes = useStyles();

    return (
        <div className={classes.root}>
            <AppBar position="static">
                <Toolbar>
                    <JenkinsButton />
                    <Title variant="h6" className={classes.title} />
                    <FullscreenButton edge="end" className={classes.menuButton} color="inherit" />
                    <SettingsButton edge="end" className={classes.menuButton} color="inherit" />
                </Toolbar>
            </AppBar>
        </div >
    );
}
