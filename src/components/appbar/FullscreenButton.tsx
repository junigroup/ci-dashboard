import IconButton, { IconButtonProps } from '@material-ui/core/IconButton';
import FullscreenIcon from '@material-ui/icons/Fullscreen';
import FullscreenExitIcon from '@material-ui/icons/FullscreenExit';
import React from 'react';
import Tooltip from '@material-ui/core/Tooltip';
import { timing } from '../../constants';

type FullscreenButtonProps = IconButtonProps;
type FullscreenButtonState = { available: boolean, fullscreen: boolean };

export default class FullscreenButton extends React.Component<FullscreenButtonProps, FullscreenButtonState> {
    constructor(props: FullscreenButtonProps) {
        super(props);
        this.state = { available: false, fullscreen: false };
    }

    componentDidMount() {
        if (document.fullscreenEnabled) {
            this.setState({
                available: true,
                fullscreen: this.isInFullscreen(),
            });

            document.onfullscreenchange = () => {
                this.setState({ fullscreen: this.isInFullscreen() });
            }
        }
    }

    handleSwitchFullscreen = () => {
        if (this.state.available) {
            if (this.isInFullscreen()) {
                document.exitFullscreen();
            }
            else {
                document.documentElement.requestFullscreen();
            }
        }
    }

    isInFullscreen = () => {
        return !!document.fullscreenElement
    }

    render() {
        return (
            <Tooltip title={this.state.fullscreen ? "exit fullscreen" : "enter fullscreen"}
                enterDelay={timing.tooltip.enterDelay} leaveDelay={timing.tooltip.leaveDelay}>
                <IconButton aria-label="fullscreen" {...this.props}
                    disabled={!this.state.available}
                    onClick={this.handleSwitchFullscreen}>
                    {this.state.fullscreen ? <FullscreenExitIcon /> : <FullscreenIcon />}
                </IconButton>
            </Tooltip>
        );
    }
}
