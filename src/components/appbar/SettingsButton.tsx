import IconButton, { IconButtonProps } from '@material-ui/core/IconButton';
import Tooltip from '@material-ui/core/Tooltip';
import SettingsIcon from '@material-ui/icons/Settings';
import React from 'react';
import SettingsDialog from '../settingsdialog';

type SettingsButtonProps = IconButtonProps;

export default function SettingsButton(props: SettingsButtonProps) {
    const [open, setOpen] = React.useState(false);

    const handleClickOpen = () => setOpen(true);
    const handleClose = () => setOpen(false);

    return (
        <React.Fragment>
            <Tooltip title="Settings">
                <IconButton {...props} aria-label="settings" onClick={handleClickOpen}>
                    <SettingsIcon />
                </IconButton>
            </Tooltip>
            <SettingsDialog open={open} onClose={handleClose} />
        </React.Fragment>
    )
}