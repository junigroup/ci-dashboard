import { Theme } from '@material-ui/core';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import TextField, { FilledTextFieldProps } from '@material-ui/core/TextField';
import { makeStyles } from '@material-ui/styles';
import React from 'react';
import { useAsync } from 'react-async-hook';
import { ping } from '../jenkins';
import { isPingAuthError, isPingUrlError } from '../jenkins/index';
import { SettingsServiceContext } from '../settings/SettingsService';
import JenkinsStatus from './JenkinsStatus';
import { Section } from './Section';

const useStyles = makeStyles((theme: Theme) => ({
    section: {
        marginBottom: "16px",
    },
    sectionHeader: {
        fontWeight: "bold",
        marginBottom: 0,
        borderBottom: "1 solid black",
    },
}));

type SettingsDialogProps = { onClose: () => void, open: boolean };
export default function SettingsDialog({ onClose, open }: SettingsDialogProps) {
    const classes = useStyles();

    return (
        <Dialog aria-labelledby="settings-dialog-title" open={open} scroll="paper" maxWidth="md" fullWidth={true}>
            <DialogTitle id="settings-dialog-title">Settings</DialogTitle>
            <DialogForm classes={classes} onClose={onClose} />
        </Dialog>);
}

function DialogForm({ ...props }) {
    const settingsService = React.useContext(SettingsServiceContext);
    const [dashboard, setDashboard] = React.useState(() => settingsService.getValues().dashboard);
    const [jenkins, setJenkins] = React.useState(() => settingsService.getValues().jenkins);
    const pingJenkins = useAsync(ping, [jenkins.url, jenkins.user, jenkins.apiToken]);

    const handleCancel = () => {
        props.onClose();
    };

    const handleSave = () => {
        settingsService.setValues({ dashboard, jenkins });
        props.onClose();
    }

    const updateDashboard = (newValues: any) => {
        setDashboard({ ...dashboard, ...newValues });
    }

    const updateJenkins = (newValues: any) => {
        setJenkins({ ...jenkins, ...newValues });
    }

    const textProps: FilledTextFieldProps = { className: props.classes.textField, margin: "normal", variant: "filled", fullWidth: true, };
    return (
        <React.Fragment>
            <DialogContent>
                <Section title="Dashboard">
                    <TextField
                        label="Titel" {...textProps} autoFocus
                        value={dashboard.title} onChange={e => updateDashboard({ title: e.target.value })}
                    />
                </Section>
                <Section title="Connection to Jenkins" error={!!pingJenkins.error}
                    hint={<JenkinsStatus loading={pingJenkins.loading} error={pingJenkins.error} />}>
                    <TextField
                        label="URL" {...textProps}
                        error={isPingUrlError(pingJenkins.error)}
                        helperText={isPingUrlError(pingJenkins.error) && pingJenkins.error.url}
                        value={jenkins.url} onChange={e => updateJenkins({ url: e.target.value })} type="url"
                    />
                    <TextField
                        label="User" {...textProps}
                        error={isPingAuthError(pingJenkins.error)}
                        helperText={isPingAuthError(pingJenkins.error) && pingJenkins.error.auth}
                        value={jenkins.user} onChange={e => updateJenkins({ user: e.target.value })}
                    />
                    <TextField
                        label="API Token" {...textProps}
                        error={isPingAuthError(pingJenkins.error)}
                        value={jenkins.apiToken} onChange={e => updateJenkins({ apiToken: e.target.value })}
                    />
                </Section>
            </DialogContent>
            <DialogActions>
                <Button onClick={handleSave} color="primary" type="submit" children="Save" />
                <Button onClick={handleCancel} children="Cancel" />
            </DialogActions>
        </React.Fragment>
    );
}
