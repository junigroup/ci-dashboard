import Badge from '@material-ui/core/Badge';
import CircularProgress from '@material-ui/core/CircularProgress';
import Tooltip from '@material-ui/core/Tooltip';
import AuthorizationFailed from '@material-ui/icons/Block';
import AuthorizationSuccess from '@material-ui/icons/Check';
import ConnectErrorIcon from '@material-ui/icons/CloudOffOutlined';
import ConnectedIcon from '@material-ui/icons/CloudOutlined';
import { makeStyles } from '@material-ui/styles';
import React from 'react';
import { PingError } from '../jenkins';
import { isPingAuthError, isPingUrlError } from '../jenkins/index';
import Box from '@material-ui/core/Box';

const useStyles = makeStyles(theme => ({
    root: {
    },
    overlay: {
        marginBottom: "12px",
        marginLeft: "12px",
    },
}));

type JenkinsStatusProps = { loading: boolean, error?: Error | PingError };
export default function JenkinsStatus({ loading, error }: JenkinsStatusProps) {
    const classes = useStyles();
    const symbol = () => {
        if (loading) {
            return (
                <CircularProgress size={20} />
            );
        } else if (isPingUrlError(error)) {
            return (
                <Tooltip title={error.url}>
                    <ConnectErrorIcon className={classes.root} />
                </Tooltip>
            );
        } else if (isPingAuthError(error)) {
            return (
                <Tooltip title={error.auth}>
                    <Badge
                        badgeContent={<AuthorizationFailed color="error" className={classes.overlay} />}
                        anchorOrigin={{ vertical: 'bottom', horizontal: 'left' }}>
                        <ConnectedIcon color="disabled" className={classes.root} />
                    </Badge>
                </Tooltip>
            );
        } else {
            return (
                <Badge
                    badgeContent={<AuthorizationSuccess color="primary" className={classes.overlay} />}
                    anchorOrigin={{ vertical: 'bottom', horizontal: 'left' }} >
                    <ConnectedIcon color="disabled" className={classes.root} />
                </Badge>
            )
        }
    };
    return (
        <Box css={{ width: 24, height: 24 }}>{symbol()}</Box>
    )
}
