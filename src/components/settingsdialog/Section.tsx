import { Theme } from '@material-ui/core';
import Box from '@material-ui/core/Box';
import DialogContentText from '@material-ui/core/DialogContentText';
import { makeStyles } from '@material-ui/styles';
import clsx from 'clsx';
import React from 'react';

const useStyles = makeStyles((theme: Theme) => ({
    root: {
        marginTop: "16px",
        marginBottom: "8px",
    },
    header: {
        fontWeight: "bold",
        marginBottom: 0,
        borderBottom: "1 solid black",
    },
    error: {
        color: theme.palette.error.main,
    },
    hint: {
        float: 'right',
    }
}));

type SectionProps = { title: string, error?: boolean, hint?: React.ReactNode, children: React.ReactNode };
export function Section({ title, error, hint, children }: SectionProps) {
    const classes = useStyles();
    return (
        <Box component="span" className={classes.root}>
            <DialogContentText component="span" className={clsx(classes.header, { [classes.error]: error })}>
                {title}{hint && <span className={classes.hint}>{hint}</span>}
            </DialogContentText>
            {children}
        </Box>
    );
}
