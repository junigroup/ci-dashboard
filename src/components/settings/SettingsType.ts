export declare type SettingsType = {
    dashboard: {
        title: string,
    },
    jenkins: {
        url: string,
        user: string,
        apiToken: string,
    }
}
