import React from 'react';
import { SettingsContext } from './SettingsService';
import { SettingsType } from './SettingsType';

/**
 * HOC to extract a setting by the specified function
 */
export function useSetting<T>(extract: (settings: SettingsType) => T) {
    const currentSettings = React.useContext(SettingsContext);
    return extract(currentSettings);
};
