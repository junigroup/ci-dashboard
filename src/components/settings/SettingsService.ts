import deepClone from 'lodash/cloneDeep';
import forEachIn from 'lodash/forOwn';
import deepMerge from 'lodash/merge';
import React from 'react';
import Engine from 'store/src/store-engine';
import LocalStorage from 'store/storages/localStorage';
import MemoryStorage from 'store/storages/memoryStorage';
import { defaultSettings } from '../../constants/defaultSettings';
import { SettingsType } from './SettingsType';

export class SettingsService {
    listeners: Set<(settings: SettingsType) => void>;
    store: StoreJsAPI;
    values: SettingsType;

    constructor(prefix: string, defaultValues: SettingsType) {
        this.listeners = new Set();
        this.values = deepClone(defaultValues);
        if (prefix) {
            this.store = Engine.createStore([LocalStorage, MemoryStorage], undefined, prefix);
            this.store.each((value, key) => (this.values as any)[key] = value);
        } else {
            this.store = Engine.createStore([MemoryStorage]);
        }
    }

    getValues(): SettingsType {
        return this.values;
    }

    setValues(settings: SettingsType) {
        this.values = deepMerge({}, this.values, settings);
        forEachIn(this.values, (value, key) => this.store.set(key, value));
        this.listeners.forEach(listener => listener(this.values));
    }

    subscribe(listener: (settings: SettingsType) => void) {
        this.listeners.add(listener);
    }

    unsubscribe(listener: (settings: SettingsType) => void) {
        this.listeners.delete(listener);
    }

}
const nullSettings: SettingsType = defaultSettings;
export const SettingsServiceContext = React.createContext<SettingsService>(new SettingsService("", nullSettings));
export const SettingsContext = React.createContext<SettingsType>(nullSettings);
