import React from 'react';
import { SettingsContext, SettingsService, SettingsServiceContext } from './SettingsService';
import { SettingsType } from './SettingsType';

const CurrentSettings = ({ service, children }: { service: SettingsService, children: React.ReactNode }) => {
    const [settings, setSettings] = React.useState(service.getValues());
    React.useEffect(() => {
        service.subscribe(setSettings);
        return () => service.unsubscribe(setSettings);
    }, [service]);
    return (
        <SettingsContext.Provider value={settings}>
            {children}
        </SettingsContext.Provider>
    );
}

type SettingsServiceProviderParams = {
    default: SettingsType,
    children: React.ReactNode,
};

export const SettingsProvider = (props: SettingsServiceProviderParams) => {
    const service = new SettingsService("ci_dashboard", props.default);
    return (
        <SettingsServiceContext.Provider value={service}>
            <CurrentSettings service={service}>
                {props.children}
            </CurrentSettings>
        </SettingsServiceContext.Provider>
    );
};
