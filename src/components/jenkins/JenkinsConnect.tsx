import React, { useContext, useEffect, useState } from 'react';
import { JenkinsAccessContext, JenkinsSettingsType, JenkinsStatus } from './JenkinsAccess';

const defaultStatus: JenkinsStatus = { connected: false };

export const JenkinsStatusContext = React.createContext<JenkinsStatus>(defaultStatus);

type JenkinsAccessProviderParams = {
    settings: JenkinsSettingsType,
    children: React.ReactNode,
}

/**
 * Use the nearest JenkinsAccess from the context and connect with the given
 * settings.
 * provides the access to the current status inclusive status messages.
 * 
 * @param settings the settings to conntect with jenkins 
 * @param children the inner react nodes
 */
export const JenkinsConnector = ({ settings, children }: JenkinsAccessProviderParams) => {
    const access = useContext(JenkinsAccessContext);
    const [status, setStatus] = useState<JenkinsStatus>(defaultStatus)
    useEffect(
        () => access.connect(settings, setStatus),
        [access, settings, setStatus]
    );
    return (
        <JenkinsStatusContext.Provider value={status}>
            {children}
        </JenkinsStatusContext.Provider>
    );
};
