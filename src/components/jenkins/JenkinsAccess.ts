import React from "react";
import Smithers from 'smithers';

export type JenkinsSettingsType = {
    url: string,
    user: string,
    apiToken: string;
}

export type JenkinsStatus = {
    connected: boolean,
    errorMessage?: string | null,
}

const nullFunction = () => { };
const unconnected: JenkinsStatus = { connected: false, errorMessage: "Waiting for connection" };

export class JenkinsAccess {
    smithers: any;
    status: JenkinsStatus;
    setStatus: (status: JenkinsStatus) => void;

    constructor() {
        this.smithers = null;
        this.status = unconnected;
        this.setStatus = nullFunction;
        this.connect = this.connect.bind(this);
        this.unconnect = this.unconnect.bind(this);
    }

    /**
     * Allows to connect with an jenkins server.
     * Only the last connect call is accepted and replace each previous connect tries

     * The status is updated after each call of the API.
     * 
     * @param {string} url the URL of the jenkins server
     * @param {string} user the user name to authorize the access
     * @param {stirng} apiToken the api token to authorize the access 
     * @param setStatus a callback for the {JenkinsStatus} of the last call
     * @returns a callback to unconnect
     */
    connect({ url, user, apiToken }: JenkinsSettingsType, setStatus: (status: JenkinsStatus) => void): () => void {
        console.log("Connect jenkins: ", url);

        this.smithers = new Smithers(url, { auth: { username: user, password: apiToken } });
        this.setStatus = setStatus;
        this.status = { connected: false };

        this.smithers.getInfo().then(
            () => this.updateStatus({ connected: true, errorMessage: null }),
            (error: Error) => this.updateStatus({ connected: false, errorMessage: error.message }),
        )

        return this.unconnect;
    }

    private unconnect() {
        console.log("Unconnect jenkins");
        this.smithers = null;
        this.status = unconnected;
        this.setStatus = nullFunction;
    }

    private updateStatus(newStatus: JenkinsStatus) {
        const nextStatus = { ...this.status, ...newStatus };
        if (this.status !== nextStatus) {
            this.status = nextStatus;
            console.log("Jenkins status: ", nextStatus)
            this.setStatus(nextStatus);
        }
    }

    isConnected() {
        return this.status.connected;
    }
}

export const JenkinsAccessContext = React.createContext(new JenkinsAccess());


