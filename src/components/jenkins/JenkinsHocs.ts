import React from "react";
import { JenkinsAccessContext } from "./JenkinsAccess";
import { JenkinsStatusContext } from "./JenkinsConnect";

/**
 * HOC to extract access to jenkins
 */
export function useJenkinsAccess(extract: (settings: any) => any) {
    const access = React.useContext(JenkinsAccessContext);
    return access;
};

/**
 * HOC to extract status of the jenkins access
 */
export function useJenkinsStatus() {
    const status = React.useContext(JenkinsStatusContext)
    return status;
};
