import Smithers from 'smithers';

export declare type PingUrlError = Error & { url: string, };
export declare type PingAuthError = Error & { auth: string, };
export declare type PingError = PingUrlError | PingAuthError;

export function isPingUrlError(error: Error | undefined): error is PingUrlError {
    return !!error && !!(error as PingUrlError).url;
}

export function isPingAuthError(error: Error | undefined): error is PingAuthError {
    return !!error && !!(error as PingAuthError).auth;
}

/**
 * Try to access the jenkins url with authorization:
 * Access `http://username:password@href`.
 * 
 * @param {string} url the href part of the URL 
 * @param {string} username the username
 * @param {string} password the password
 * @returns {Promise<void>} null or rejects with PingError
 */
export const ping = (url: string, username: string, password: string): Promise<void> => {
    const failed = (problem: { url: string } | { auth: string }) => (error: Error) =>
        Promise.reject({ message: error.message, ...problem, stack: error.stack });
    return Promise.resolve(url)
        .then(() => new URL(url)).catch(failed({ url: 'Invalid URL' }))
        .then(() => fetch(url, { method: 'HEAD', mode: 'cors', credentials: 'omit' }).catch(failed({ url: 'Not reached' })))
        .then(() => new Smithers(url, { auth: { username, password } }).getInfo({ params: { tree: 'mode' } }).catch(failed({ auth: 'Authorization failed' })))
        .then(() => undefined);
}
