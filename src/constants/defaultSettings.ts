import { SettingsType } from '../components/settings/SettingsType';

export const defaultSettings: SettingsType = {
    dashboard: {
        title: "CI Dashboard",
    },
    jenkins: {
        url: "http://localhost:8080",
        user: "",
        apiToken: ""
    }
}
